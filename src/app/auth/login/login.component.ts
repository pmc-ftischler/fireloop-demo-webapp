import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserApi } from '../../shared/sdk/services/custom/User';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-login-component',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm: FormGroup;

  constructor(fb: FormBuilder,
              private appRouter: Router,
              private userApi: UserApi,
              public snackBar: MdSnackBar) {
    this.loginForm = fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  doLogin() {
    this.userApi.login({email: this.loginForm.value.login, password: this.loginForm.value.password})
      .subscribe(
        (data: any) => {
          this.appRouter.navigate(['todos']);
          // console.log(data);
        }, (error: any) => {
          // console.log(error);
          const config = new MdSnackBarConfig();
          config.duration = 1000;
          this.snackBar.open(error.message, null, config);
      });
  }
}
