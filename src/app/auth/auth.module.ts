import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { UserApi } from '../shared/sdk/services/custom/User';
import { MdButtonModule, MdCardModule, MdSnackBarModule, MdInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RecoverComponent } from './recover/recover.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    MdCardModule,
    MdButtonModule,
    MdInputModule,
    MdSnackBarModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  declarations: [
    LoginComponent,
    RecoverComponent,
    RegisterComponent
  ],
  providers: [
    UserApi
  ]
})
export class AuthModule { }
