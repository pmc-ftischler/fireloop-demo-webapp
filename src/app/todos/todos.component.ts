import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { Todo } from '../shared/sdk/models/Todo';
import { FireLoopRef } from '../shared/sdk/models/FireLoopRef';
import { RealTime } from '../shared/sdk/services/core/real.time';
import { LoopBackConfig } from '../shared/sdk/lb.config';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
  public realTimeReady$: Observable<any>;
  public todos$: Observable<Todo | Todo[]>;
  public formGroup: FormGroup = new FormGroup({
    title: new FormControl(''),
    done: new FormControl(false)
  });
  private todoRef: FireLoopRef<Todo>;

  constructor(private realTime: RealTime) {
    LoopBackConfig.setBaseURL('http://localhost:3000');
  }

  ngOnInit(): void {
    this.realTimeReady$ = this.realTime.onReady()
      .do(() => this.todoRef = this.realTime.FireLoop.ref<Todo>(Todo))
      .do(() => this.todos$ = this.todoRef.on('change'));
  }

  onSubmit(): void {
    const title: string = this.title.value;
    const done: boolean = this.done.value || false;
    const newTodo: Todo = new Todo({title, done});
    console.log(newTodo);
    this.todoRef.create(newTodo).subscribe(() => this.formGroup.reset());
  }

  get title(): AbstractControl {
    return this.formGroup.get('title');
  }

  get done(): AbstractControl {
    return this.formGroup.get('done');
  }
}
