import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserApi } from '../shared/sdk/services/custom/User';

@Injectable()
export class IgnoreAuthGuard {

  constructor(private appRouter: Router, private userApi: UserApi) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.userApi.isAuthenticated()) {
      this.appRouter.navigate(['todos']);
    } else {
      // Is not authenticates, can go to auth-pages
      return true;
    }
  }
}
