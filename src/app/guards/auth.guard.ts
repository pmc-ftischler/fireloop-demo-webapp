import {UserApi} from '../shared/sdk/services/custom/User';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';

@Injectable()
export class AuthGuard {

  constructor( private userApi: UserApi, private router: Router ) {
  }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
    if (this.userApi.isAuthenticated()) {
      console.log('LOGGED IN');
      return true;
    } else {
      console.log('NOT LOGGED IN');
      this.router.navigate(['auth']);
    }
  }
}
